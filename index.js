const express = require('express');
const mongoose = require('mongoose');
const http = require('http');
const jwt = require('jsonwebtoken');
const Status = require('./utils').Status;
const router = require('./router');
const bodyParser = require('body-parser');
const logger = require('./utils').logger;

const app = express();

const port = 6050;
app.use(logger);
app.use('/api', router);
app.use('/', express.static('static'));

const server = http.createServer(app);

server.listen(port, '0.0.0.0', () => console.log(`Listening on port ${port}`));