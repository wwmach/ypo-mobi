const mongoose = require('mongoose');
const ENV = require('./env');

const populateRooms = require('./models/populateData/availableRooms');
const populateFlights = require('./models/populateData/availableFlights');

mongoose.Promise = global.Promise;
mongoose.connect(ENV.mongoLocation, { useMongoClient: true });

console.log('Initializing Database');
populateRooms();
populateFlights();
