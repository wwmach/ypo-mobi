var page = 0;
var global = {
	rooms: [],
	flights: [],
	hotelX: 60,
	hotelScrollRange: 600
}

var form = {
	guestCount: 2,
	guests: [],
	hotelRoom: '',


}

/**
 * 	Navigation
 */

function setActiveIcon() {
	var current = document.querySelector('.header .active');
	current.classList.remove('active');
	current.querySelectorAll('path').forEach(function(p) {
		p.style.fill = '#918f90';
	})
	var icons = document.querySelectorAll('.header .icon');
	icons[page].classList.add('active');
	icons[page].querySelectorAll('path').forEach(function(p) {
		p.style.fill = '#3871c1';
	})
}

function next() {
	var container = document.getElementById('container');
	page++;
	if (page>0) {
		var back = document.getElementById('back');
		back.style.display = 'inline-block';
	}
	if (page <4) {
	container.style.transform = 'translateX(-'+(100*page)+'%)';
	} else {
		console.log('end of form reached');
	}
	setActiveIcon()
}

function back() {
	var container = document.getElementById('container');
	page--;
	if (page<=0) {
		var back = document.getElementById('back');
		back.style.display = 'none';
	}
	if (page >= 0) {
		container.style.transform = 'translateX(-'+(100*page)+'%)';
	}
	setActiveIcon()
}


/**
 * 	End Navigation
 */

/**
 * 	Hotel Builder
 */

function hotelBack() {
	var container = document.getElementById('roomContainer');
	var next = document.getElementById('hotel-next');
	var back = document.getElementById('hotel-back');
	global.hotelX+= global.hotelScrollRange;
	next.classList.remove('disabled');
	if (global.hotelX >= 300) {
		global.hotelX = 300;
		back.classList.add('disabled');
	}
	console.log(global.hotelX);
	console.log(container);
	container.style.transform = 'translateX('+global.hotelX+'px)';
}

function hotelNext() {
	var container = document.getElementById('roomContainer');
	var next = document.getElementById('hotel-next');
	var back = document.getElementById('hotel-back');
	back.classList.remove('disabled');
	global.hotelX-= global.hotelScrollRange;
	if (global.hotelX <= -1150) {
		global.hotelX = -1150;
		next.classList.add('disabled');
	}
	console.log(global.hotelX);
	console.log(container);
	container.style.transform = 'translateX('+global.hotelX+'px)';

}

function populateRooms(rooms) {
	global.rooms = rooms;
	var container = document.getElementById('roomContainer');
	container.innerHTML = rooms.map(buildRoomBlock).join('');
}
function selectRoom(room) {
	var key = hotel.name.replace(/[\s|'|"]/g, '');
	form.hotelRoom = room;
	var oldChecked = document.querySelector('#hotel input:checked');
	if (oldChecked) {
	oldChecked.checked = false;
	}
	console.log('check', room);
	document.getElementById(key).checked = true;
}

function buildRoomBlock(hotel) {
	var key = hotel.name.replace(/[\s|'|"]/g, '');
	return (
		"<div class='room' onclick='selectRoom(\""+hotel.name+"\")'>"+
			"<div class='roomDetail'>"+
				"<img src='https://media-cdn.tripadvisor.com/media/photo-s/02/37/27/00/hotel-bed.jpg'>"+
				"<table>"+
					"<tbody>"+
						"<tr><th colspan='2'>"+hotel.name+"</th></tr>"+
						"<tr><td>Room Size:</td><td>"+'226 sq. ft'+"</td></tr>"+
						"<tr><td>Bed Size:</td><td>"+'queen'+"</td></tr>"+
						"<tr><td>Floorplan</td><td>View</td>"+
					"</tbody>"+
				"</table>"+
			"</div>"+
			"<div class='dangling-select'>"+
				"<div class='checkbox-container'><input id='"+key+"' type='checkbox'></div><label for='"+key+"'>Select this room</label></div>"+
			"</div>"+
		"</div>"
	)
}


/**
 * 	Initialization
 */
window.onload = function() {
	document.getElementById('next').addEventListener('click', next);
	document.getElementById('back').addEventListener('click', back);
	document.getElementById('hotel-back').addEventListener('click', hotelBack);
	document.getElementById('hotel-next').addEventListener('click', hotelNext);
	fetch('/api/availableRooms').then(function(request) { return request.json()}).then(function(data) { populateRooms(data)})
	fetch('/api/availableFlights').then(function(request) { return request.json()}).then(function(data) { console.log()})
	global.hotelScrollRange = Math.min(600, window.innerWidth/2);
}