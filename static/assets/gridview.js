
function getData() {
	let chain = fetch('/api/rsvp/all', {
		headers: {
			contentType: 'application/json',
			authorization: localStorage.getItem('authorization')
		}
	}).then(i => {
		if (!i.ok) {
			throw i;
		}
		return i.json();
	})
	.catch(i => window.location.href = '/login.html')
	return chain;
}

window.onload = e => {
	let root = document.getElementById('gridRoot');
	getData()
		.then(i => {
			console.log(i);
			let html = i.sort((a, b) => a.name === b.name? 0 : a.name < b.name? -1 : 1)
				.map(i => `<tr><td>${i.name}</td><td>${i.phone}</td><td>${i.email}</td><td>${i.guests}</td><td>${i.eaname||''}</td><td>${i.eaphone||''}</td><td>${i.eaemail||''}</td><td>${i.coordinatewithea? 'Yes' : 'No'}</td><tr>`)
				.join('')
			root.innerHTML = html;
		})
}