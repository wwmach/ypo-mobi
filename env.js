const environment = {
	mongoLocation: "mongodb://evergreen:evergreen@exceptionallyrecursive.com:27017/ypo",
	secret: "keyboardcat",
	authToken: "jwtToken",
	auth: {
		users: [
			'asparks@wwmach.com',
			'rlimbacher@cctrvl.com',
			'evan@wwmach.com',
			'egreenberg@wwmach.com',
			'lllowery@aol.com',
			'esalman@derrick.com',
			'rfrye@cockrell.com', // Rhonda Frye
			'aedmundson@hooversolutions.com',
			'admin'
		],
		password: 'evergreen2986'
	},
	mail: {
		host: 'outlook.wwmach.com', //'4.16.113.249'
		port: 25,
		secure: false,
		tls: { rejectUnauthorized: false}
	}
}


module.exports = environment;