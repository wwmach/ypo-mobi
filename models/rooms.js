const mongoose = require('mongoose');

const Room = new mongoose.Schema({
	name: String,
	price: Number,
	count: Number,
})

module.exports = mongoose.model('Room', Room);