const mongoose = require('mongoose');
const Flight = require('../flights');

const flights = [
	{
		airline: 'British Airways',
		flightNumber: 'BA0196',
		class: 'J-Business',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal D International',
		destination: 'London, Heathrow Airport - Terminal 5',
		departureTime: 'Tue 17 Apr 08:25PM',
		arrivalTime: 'Wed 18 Apr 11:45AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'British Airways',
		flightNumber: 'BA0195',
		class: 'J-Business',
		origin: 'London, Heathrow Airport - Terminal 5',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Sun 22 Apr 09:55AM',
		arrivalTime: 'Sun 22 Apr 02:15PM',
		planeModel: 'Boeing 777',
		time: '10h20m',
		seats: 20
	},
	{
		airline: 'British Airways',
		flightNumber: 'BA0194',
		class: 'J-Business',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal D International',
		destination: 'London, Heathrow Airport - Terminal 5',
		departureTime: 'Tue 17 Apr 04:05PM',
		arrivalTime: 'Wed 18 Apr 07:25AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'British Airways',
		flightNumber: 'BA197',
		class: 'J-Business',
		origin: 'London, Heathrow Airport - Terminal 5',
		destination: 'Houston George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Sun 22 Apr 02:20PM',
		arrivalTime: 'Sun 22 Apr 06:35PM',
		planeModel: 'Boeing 777',
		time: '10h15m',
		seats: 20
	},
	{
		airline: 'United',
		flightNumber: 'UA0880',
		class: 'P-Business/busfirst',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Tue 17 Apr 04:20PM',
		destination: 'London, Heathrow Airport - Terminal 2',
		arrivalTime: 'Wed 18 Apr 07:40AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'United',
		flightNumber: 'UA0879',
		class: 'Z-Business/busfirst',
		origin: 'London, Heathrow Airport - Terminal 2',
		departureTime: 'Sun 22 Apr 09:30AM',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		arrivalTime: 'Sun 22 Apr 01:55PM',
		planeModel: 'Boeing 777',
		time: '10h25m',
		seats: 20
	},
	{
		airline: 'United',
		flightNumber: 'UA0005',
		class: 'P-Business/busfirst',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Tue 17 Apr 08:10PM',
		destination: 'Londone Heathrow Airport - Terminal 2',
		arrivalTime: 'Wed 18 Apr 11:35AM',
		planeModel: 'Boeing 777',
		time: '09h25m',
		seats: 20
	},
	{
		airline: 'United',
		flightNumber: 'UA0004',
		class: 'Z-Business/busfirst',
		origin: 'London, Heathrow Airport - Terminal 2',
		departureTime: 'Sun 22 Apr 01:40PM',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		arrivalTime: 'Sun 22 Apr 06:00PM',
		planeModel: 'Boeing 777',
		time: '10h20m',
		seats: 20
	}
]

module.exports = () => {
	Flight.remove({}, (err) => {
		if (err) return console.error(err);
		process.stdout.write('Clearing Flights collection\n');
		flights.forEach((flight, i) => {
			process.stdout.cursorTo(0);
			process.stdout.write(`Populating Flights ${i+1}/${flights.length}`);
			let newFlight = new Flight(flight);
			newFlight.save((err, flight) => {
				if (err) console.error(err, room);
			});
		});
		process.stdout.write('\n');
	});
}