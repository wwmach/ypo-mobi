const mongoose = require('mongoose');
const Room = require('../rooms');

const rooms = [
	{ count: 27, name: 'Deluxe King Room', price: 625 },
	{ count:  4, name: 'Superior Junior Suite', price: 700 },
	{ count:  3, name: 'Deluxe Junior Suite', price: 825 },
	{ count:  1, name: 'Prince\'s Lodge Suite', price: 1150 },
	{ count:  3, name: 'Carlos Suite', price: 1225 },
	{ count:  1, name: 'Adam\'s Suite', price: 1325 },
	{ count:  5, name: 'Grosvenor Suite', price: 1400 },
	{ count:  2, name: 'Connaught Suite', price: 1900 },
	{ count:  1, name: 'Sutherland Suite', price: 2100 }
]

module.exports = () => {
	Room.remove({}, (err) => {
		if (err) { return console.error(err); }
		process.stdout.write('Clearing Rooms collection\n');
		rooms.forEach((room, i) => {
			process.stdout.cursorTo(0);
			process.stdout.write(`Populating Rooms ${i+1}/${rooms.length}`);
			let newRoom = new Room(room);
			newRoom.save((err, room) => {
				if (err) console.error(err, room);
			});
		});
		process.stdout.write('\n');
	});
}