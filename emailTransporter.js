const nodemailer = require('nodemailer');
const ENV = require('./env');

const sendEmail = (recipient, body) => {
    nodemailer.createTestAccount((err, account) => {
        let transporter = nodemailer.createTransport({
            host: 'outlook.wwmach.com',
            port: 25, //465
            secure: false,//true
            tls: { rejectUnauthorized: false /*ciphers: 'SSLv3'*/ },
            // auth: {
            //     user: 'no-reply@YPOLondonCalling2018.com',
            //     pass: ''
            // }
            // auth: account,
        })

        let mailOptions = {
            from: 'YPO London Calling 2018 <no-reply@YPOLondonCalling2018.com>',
            to: recipient,
            subject: 'YPO London Couples Retreat',
            text: '',
            html: body
        };

        transporter.sendMail(mailOptions, (err, info) => {
            if (err) {
                return console.error(err);
            }
            console.log(info);
        })
    })
}

// const transporter = nodemailer.createTransport(ENV.mail);

module.exports = sendEmail;