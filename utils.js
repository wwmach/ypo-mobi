const ENV = require('./env');
const jwt = require('jsonwebtoken');

const formioParser = (req, res, next) => {
		console.log(JSON.parse(req.body).form.data);
	let formData = JSON.parse(req.body).form.data
		.map(f => [f[0].toLowerCase(), f[1]])
		.reduce( (a, b) => ({...a, [b[0]]: b[1]}), {});
		req.body = formData;
	next();
}

const Status = {
	deny: 401,
	error: 500,
	ok: 200,
	badRequest: 400
}

const auth = (req, res, next) => {
	const auth = req.get('authorization');
	if (!auth) res.sendStatus(Status.deny);
	jwt.verify(auth, ENV.secret, (err, decoded) => {
		if (err) res.sendStatus(Status.deny);
		else next();
	})
}

const verifyLogin = (req, res, next) => {
	if (req.body.password === ENV.auth.password) {
		let username = req.body.name.toLowerCase();
		if (ENV.auth.users.includes(username)) {
			return next();
		}
	}
	res.status(Status.deny).send('Login failed, please try again');
}

const logger = (req, res, next) => {
	console.log(req.url);
	next();
}

module.exports = { Status, auth, formioParser, logger, verifyLogin };