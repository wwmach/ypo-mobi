const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const sanitize = require('mongo-sanitize');
const express = require('express');
const jwt = require('jsonwebtoken');

// Mongoose Models
const Respondent = require('./models/respondents');
const Room = require('./models/rooms');
const Flight = require('./models/flights');

//  Utilities
const { auth, Status, formioParser, verifyLogin } = require('./utils');

// Environment Variables
const ENV = require('./env');

// Email Templates
const confirmationEmail = require('./static/assets/emailTemplates/confirmation');
const notificationEmail = require('./static/assets/emailTemplates/notification');

// Email Delivery
const sendEmail = require('./emailTransporter');

const router = express.Router();

mongoose.Promise = global.Promise;
mongoose.connect(ENV.mongoLocation, { useMongoClient: true });
const jsonParser = bodyParser.json();
const formParser = bodyParser.text({type: 'urlencoded'});

router.post('/register', formParser, formioParser, (req, res) => {
  let user = sanitize(req.body);
  // req.body.coordinatwwithea = req.body.coordinatwwithea === 'on';
  let newRsvp = new Respondent(user);
  newRsvp.save((err, rsvp) => {
    if (err) res.status(Status.badRequest).send('{ "ok": "false"}');
    else res.status(Status.ok).send('{ "ok": "true"}');
    sendEmail(req.body.email, confirmationEmail(req.body));
    sendEmail(ENV.auth.users, notificationEmail(req.body));
  });
});

router.post('/auth', formParser, formioParser, verifyLogin,  (req, res) => {
  jwt.sign({ data: 'woo'}, ENV.secret, { expiresIn: '1hr'}, (err, token) => {
    if (err) res.status(Status.error).send('{}');
    else {
      res.set('authorization', token);
      res.status(Status.ok).send('Login Success');
    }
  });
})

router.post('/rsvp/signup', jsonParser, (req, res) => {
  let sanitizedBody = sanitize(req.body);
  let newRsvp = new Respondent(sanitizedBody);
  newRsvp.save((err, rsvp) => {
    if (err) res.sendStatus(Status.error);
    res.sendStatus(Status.ok);
  })
})

router.get('/rsvp/all', auth, (req, res) => {
  Respondent.find({}, (err, users) => {
    if (err) res.sendStatus(Status.error);
    res.json(users);
  })
});

router.get('/availableRooms', (req, res) => {
  Room.find({ count: { $gt: 0}}, (err, rooms) => {
    if (err) res.sendStatus(Status.error).send(error);
    res.json(rooms);
  })
});

router.get('/availableFlights', ( req, res) => {
  Flight.find({ seats: { $gt: 0}}, (err, flights) => {
    if (err) res.sendStatus(Status.error).send(error);
    res.json(flights);
  })
});



module.exports = router;